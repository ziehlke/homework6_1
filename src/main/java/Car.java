import java.time.LocalDate;

public class Car {
    private CarBrand marka;
    private String nazwa;
    private CarType typ;
    private double spalanie;
    private double wielkośćSilnika;
    private int ilośćMiejsc;
    private int mocSilnika;
    private LocalDate dataWyprodukowania;
    private boolean automat;
    private Naped naped;


    public Car(CarBrand marka, String nazwa, CarType typ, double spalanie, double wielkośćSilnika, int ilośćMiejsc, int mocSilnika, LocalDate dataWyprodukowania, boolean automat, Naped naped) {
        this.marka = marka;
        this.nazwa = nazwa;
        this.typ = typ;
        this.spalanie = spalanie;
        this.wielkośćSilnika = wielkośćSilnika;
        this.ilośćMiejsc = ilośćMiejsc;
        this.mocSilnika = mocSilnika;
        this.dataWyprodukowania = dataWyprodukowania;
        this.automat = automat;
        this.naped = naped;
    }

    public Car() {
    }

    public CarBrand getMarka() {
        return marka;
    }

    public void setMarka(CarBrand marka) {
        this.marka = marka;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public CarType getTyp() {
        return typ;
    }

    public void setTyp(CarType typ) {
        this.typ = typ;
    }

    public double getSpalanie() {
        return spalanie;
    }

    public void setSpalanie(double spalanie) {
        this.spalanie = spalanie;
    }

    public double getWielkośćSilnika() {
        return wielkośćSilnika;
    }

    public void setWielkośćSilnika(double wielkośćSilnika) {
        this.wielkośćSilnika = wielkośćSilnika;
    }

    public int getIlośćMiejsc() {
        return ilośćMiejsc;
    }

    public void setIlośćMiejsc(int ilośćMiejsc) {
        this.ilośćMiejsc = ilośćMiejsc;
    }

    public int getMocSilnika() {
        return mocSilnika;
    }

    public void setMocSilnika(int mocSilnika) {
        this.mocSilnika = mocSilnika;
    }

    public LocalDate getDataWyprodukowania() {
        return dataWyprodukowania;
    }

    public void setDataWyprodukowania(LocalDate dataWyprodukowania) {
        this.dataWyprodukowania = dataWyprodukowania;
    }

    public boolean isAutomat() {
        return automat;
    }

    public void setAutomat(boolean automat) {
        this.automat = automat;
    }

    public Naped getNaped() {
        return naped;
    }

    public void setNaped(Naped naped) {
        this.naped = naped;
    }
}
