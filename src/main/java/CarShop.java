import java.util.List;

public class CarShop {
    String nazwa;
    List<CarOffer> listaOfert;

/*    Stwórz metody:
            - metodę z varargs, która jako parametr przyjmuje ofertę samochodową
            (różną ilość ofert samochodowych) i dodaje wszystkie obiekty przekazane w parametrze
            do listy ofert podanego sklepu.*/

    public void addOffers(CarOffer... carOffers) {

        for (CarOffer carOffer : carOffers) {
            listaOfert.add(carOffer);
        }
    }
}
